
import React,  {useEffect , useState } from 'react';
import Table from './Table'
import axios from 'axios'



const API_LOGIN = process.env.REACT_APP_API_LOGIN;
const API_PASSWORD = process.env.REACT_APP_API_PASSWORD;
const API_BASE_URL = process.env.REACT_APP_API_BASE_URL;

const DataFetch = () => {
    const [devis, setDevis] = useState({})
    const [factures, setFactures] = useState({})
    const [loading, setloading] = useState(true);

    useEffect(() => {
        getData()
    }, []);


    
  async function getData() {
    const token = `${API_LOGIN}:${API_PASSWORD}`;
    const encodedToken = Buffer.from(token).toString("base64");
    const headers = { Authorization: "Basic " + encodedToken };

    await axios.get(`${API_BASE_URL}/quotes.json`, { headers })
      .then((res) => {
        setDevis({data:res.data})
        setloading(false)
      })
      .catch(error => {
       console.log(error)
      });

    await axios.get(`${API_BASE_URL}/invoices.json`, { headers })
      .then((res) => {
        setFactures({data:res.data})
        setloading(false)
      })
      .catch(error => {
       console.log(error)
      });
    
  }



  if (loading) {
    return (
      <div className="spinner-border text-primary" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    );
  } else {
    return (
        <Table devis={devis} factures={factures}/>
     );
  }
}


export default DataFetch;
