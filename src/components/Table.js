import React from 'react';


const Table = (props) => {

  console.log(props)


  function convertStatusToText(status) {
    switch (status) {
      case 0:
        return 'En attente'
      case 1:
        return 'Accepté'
      case 9:
        return 'Refuse'
      default:
        break;
    }
  }

  function convertColorStatus(status) {
    switch (status) {
      case 0:
        return 'orange'
      case 1:
        return 'green'
      case 9:
        return 'red'
      default:
        break;
    }
  }


  function convertMb(prix_vente,cout_achat){
    return ((prix_vente - cout_achat) / cout_achat)  * 100
  }

  function countAllVente(){
    let total = 0

    props.devis.data.forEach(d => total += parseInt(d.amount_invoiced) )

    return total
  }


  function countAllMb(){
    let totalCout = 0
    let totalVente = 0

    props.devis.data.forEach((d) => {
      if (d.fully_invoiced) {
        totalCout += parseInt(d.total)
        totalVente += parseInt(d.amount_invoiced)
      }
    } )

    return convertMb(totalVente,totalCout)
  }





  return (
    <div>
      <h1> Suivie budget </h1>
      <h4>Vente total : {countAllVente()}€ </h4>
      <h6>Marge brut total : {countAllMb()}€ </h6>
      <table className="table table-striped table-bordered" id="table-budget">
        <thead>
          <tr>
            <th> Client </th>
            <th> Objet</th>
            <th> N° devis</th>
            <th>Status</th>
            <th>CA HT</th>
            <th>Total achats HT</th>
            <th>Devis PDF</th>
            <th>MB %</th>
            <th>MB €</th>
          </tr>
        </thead>
        <tbody>
          {props.devis.data.map((d) => (
            <tr key={d.id}>
              <td>{d.customer_identity}</td>
              <td>{d.title}</td>
              <td>{d.quote_ref}</td>
              <td className={convertColorStatus(d.quote_status)}>{convertStatusToText(d.quote_status)}</td>
              <td>{d.total}€</td>
              <td>{d.fully_invoiced ? d.amount_invoiced : "0"}€</td>
              <td><a href={d.public_download_url}>Télécharger devis</a></td>
              <td> {d.fully_invoiced ? convertMb(d.total,d.amount_invoiced): 0} %</td>
              <td>{d.fully_invoiced ? d.total - d.amount_invoiced : 0}€</td>
            </tr>
          ))}
        </tbody>
        <tfoot>
          <tr>
          <th> Client </th>
          <th> Objet</th>
          <th> N° devis</th>
          <th>Status</th>
          <th>CA HT</th>
          <th>Total achats HT</th>
          <th>Devis PDF</th>
          <th>MB %</th>
          <th>MB €</th>
          </tr>
        </tfoot>
      </table>
    </div>

  );
}



export default Table;
