$(document).ready(function() {
    $('#table-budget').DataTable({
        "language":{
            "decimal":        "",
            "emptyTable":     "No data available in table",
            "info":           "Element de _START_ à _END_ sur _TOTAL_ éléments",
            "infoEmpty":      "Showing 0 to 0 of 0 entries",
            "infoFiltered":   "(filtered from _MAX_ total entries)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Afficher _MENU_ élements",
            "loadingRecords": "Chargement...",
            "processing":     "En cours...",
            "search":         "Rechercher:",
            "zeroRecords":    "No matching records found",
            "paginate": {
                "first":      "Premier",
                "last":       "Dernier",
                "next":       "Suivant",
                "previous":   "Précedent"
            },
            "aria": {
                "sortAscending":  ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            }
        },
        "responsive": true
    });
} );